import pygame
import random
import matplotlib.pyplot as plt
import numpy as np
import sys


class GUI():  # Handles all the GUI
    pygame.init()
    clock = pygame.time.Clock()
    FPS = 24
    Display = pygame.display.set_mode((500, 400))
    image = pygame.image.load("UICharacterSheet.png")
    imageData = image.convert()
    characterSheet = pygame.transform.scale(image, (int(imageData.get_width() / 2), int(imageData.get_height() / 2)))

    def printText(message, position, underLine, Colour, Size):
        fontName = pygame.font.match_font('bodoni')
        printedMessage = pygame.font.Font(fontName, Size).render(message, True, Colour)
        BackdropMessage = pygame.font.Font(fontName, Size).render(message, True, (0, 0, 0))
        BackdropMessage.set_alpha(100)
        if underLine:
            pygame.draw.line(GUI.Display, (0, 156, 86), (1, Size), (21 * len(message), Size), 3)
        GUI.Display.blit(BackdropMessage, (position[0] + 2, position[1] + 2))
        GUI.Display.blit(printedMessage, position)

    def printImage(image, position, cropArea, topLeftOrBottomRight):
        if topLeftOrBottomRight == "tl":
            chopSize = cropArea
            GUI.Display.blit(pygame.transform.chop(image, chopSize), position)
        elif topLeftOrBottomRight == "br":
            chopSize = cropArea
            croppedImage = pygame.transform.chop(pygame.transform.flip(image, True, True), chopSize)
            GUI.Display.blit(pygame.transform.flip(croppedImage, True, True), position)
    def MainScreenButtons(but1, but2):
      GUI.printImage(pygame.image.load("UIItems.png"), (but1,100), (0,0,355,224-96), "br")
      GUI.printImage(pygame.image.load("UIItems.png"), (but2,200), (0,0,355,0), "tl")
      GUI.printText("Start Simulation", (but1 + 30,134), False,(0,0,0),30)
      GUI.printText("Quit Application", (but2 + 30,234), False, (0,0,0),30)
      GUI.printImage(pygame.image.load("UIItems.png"),(350, 250), (0,0,128,96), "tl")
      GUI.printText("Monty.py", (375,330), False, (255,255,0),20)

    def ApplicationLoop():
        running = True
        but1 = -100
        but1Spd = 0
        but2 = -200
        time = 0
        selectedElement = ""
        intro = True
        while running:
            GUI.Display.fill((230, 255, 230))
            GUI.printText("Monte Carlo Simulation", (0, 0), True, (0, 156, 86), 46)
            mousePos = pygame.mouse.get_pos()
            time += 1
            if intro:
                if but1 < -20:
                    but1 += but1Spd
                    but1Spd += 1
                elif but2 < -20:
                    but2 += but1Spd
                    but1Spd += 1
                if time >= 24:
                    intro = False
                GUI.MainScreenButtons(but1,but2)
            else:
                for event in pygame.event.get():
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_ESCAPE:
                            running = False
                if selectedElement == "P":
                    GUI.StartSimGUI()
                    break
                if mousePos[0] < 355 and mousePos[0] > 0 and mousePos[1] < 196 and mousePos[1] > 100:
                    if but1 < 0:
                        but1 += 5
                    if but2 > -20:
                        but2 -= 2
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        selectedElement = "P"
                elif mousePos[0] < 355 and mousePos[0] > 0 and mousePos[1] < 296 and mousePos[1] > 200:
                    if but2 < 0:
                        but2 += 5
                    if but1 > -20:
                        but1 -= 2
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        selectedElement = "Q"
                if selectedElement == "Q":
                    running = False
                GUI.MainScreenButtons(but1,but2)
            pygame.display.flip()
            GUI.clock.tick(GUI.FPS)
        pygame.quit()
        sys.exit()

    def StartSimGUI():
        running = True
        iterations = ""
        doors = ""
        selected = 0
        answer = ""
        time = 0
        numberOfSpeech = 2
        SpeechBubble = pygame.image.load("UIItems.png")
        speechBubbleTime = 0
        while running:
            GUI.Display.fill((230, 255, 230))
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False
                    elif event.key == pygame.K_DOWN:
                        if selected < 1:
                            selected += 1
                    elif event.key == pygame.K_UP:
                        if selected > 0:
                            selected -= 1
                    elif event.key == pygame.K_RETURN and doors != "" and iterations != "":
                        a = MonteCarloSimulation(int(iterations))
                        if int(doors) == 3:
                            answer = str(a.GameShowSim())
                            GUI.printText("The experimental", (1, 0), False, (0, 0, 255), 30)
                            GUI.printText("probability of success", (1, 30), False, (0, 0, 255), 30)
                            GUI.printText("if you stick is: ", (1, 60), False, (0, 0, 255), 30)
                            GUI.printText(answer, (190, 60), False, (0, 0, 0), 30)
                            pygame.display.flip()
                            a.graphResults()
                        else:
                            percent = a.resultsNdoors(int(doors))
                            GUI.printText("The percentage", (1, 0), False, (0, 0, 255), 30)
                            GUI.printText("error is:", (1, 30), False, (0, 0, 255), 30)
                            GUI.printText(percent, (1, 60), False, (0, 0, 0), 30)
                            pygame.display.flip()
                            a.graphN(int(doors))

                    if str(pygame.key.name(event.key)).isdigit():
                        if selected == 0 and len(iterations) <= 9:
                            iterations += str(pygame.key.name(event.key))
                        elif selected == 1:
                            if len(doors) <= 5:
                                doors += str(pygame.key.name(event.key))
                    elif event.key == pygame.K_BACKSPACE:
                        if selected == 0:
                            if iterations != "":
                                iterations = iterations[:-1]
                        else:
                            if doors != "":
                                doors = doors[:-1]
            if selected == 0:
                pygame.draw.line(GUI.Display, (0, 0, 255), (1, 40), (370, 40), 3)
                pygame.draw.line(GUI.Display, (0, 0, 255), (25 * len(iterations) + 2, 50),
                                 (25 * len(iterations) + 2, 100), 3)
            else:
                pygame.draw.line(GUI.Display, (0, 0, 255), (1, 140), (300, 140), 3)
                pygame.draw.line(GUI.Display, (0, 0, 255), (25 * len(doors) + 2, 150), (25 * len(doors) + 2, 200), 3)
            GUI.printImage(SpeechBubble,(240, 125), (150,0,250,96), "tl")
            GUI.printText("Number of iterations:", (0, 0), False, (0, 0, 255), 40)
            GUI.printText(iterations, (0, 50), False, (0, 0, 255), 50)
            GUI.printText("Number of doors:", (0, 100), False, (0, 0, 255), 40)
            GUI.printText(doors, (0, 150), False, (0, 0, 255), 50)
            GUI.printImage(GUI.characterSheet, (350, 100), (0, 0, 134, 0), "br")
            if time == 0:
                GUI.printImage(GUI.characterSheet, (350, 100), (0, 0, 134, 0), "br")
            elif time == 1 or time == 3:
                GUI.printImage(GUI.characterSheet, (350, 100), (0, 0, 134, 0), "tl")
            elif time == 2:
                GUI.printImage(GUI.characterSheet, (350, 100), (0, 0, 0, 325), "tl")
            pygame.display.flip()
            if numberOfSpeech > 0:
                if time >= 9:
                    time = 0
                    numberOfSpeech -= 1
                else:
                    time += 1
            else:
                if speechBubbleTime < 255:
                    speechBubbleTime += 5
            SpeechBubble.set_alpha(255 - speechBubbleTime)
            GUI.clock.tick(GUI.FPS)
        pygame.quit()
        sys.exit()


class MonteCarloSimulation():  # Handles all the calculations
    def __init__(self, numberOfSimulations):
        self.countOfWins = 0
        self.numberOfSimulations = numberOfSimulations
        self.averagesPerIteration = []

    def graphResults(self):  # graphs results of 3 door
        expectedResult = 1 / 3
        length = len(self.averagesPerIteration)
        plt.figure(1)
        plt.plot(self.averagesPerIteration, label="Results")  # plots averages against iterations
        plt.plot([expectedResult for i in range(length)], label="Expected")  # plots the line y = 1/3 ie the expected
        plt.xlabel("Iterations")
        plt.ylabel("Probability of success if stick")
        plt.legend()
        plt.show()

    def resultsNdoors(self, DoorsLimit):  # n door graph
        self.probabilityFound = []
        for numberOfDoors in range(3, DoorsLimit + 1):
            stay = 0
            for i in range(self.numberOfSimulations):
                winningDoor = random.randint(0, numberOfDoors - 1)
                doors = [0 for i in range(numberOfDoors)]
                doors[winningDoor] = 1
                computerChoice = random.randint(0, numberOfDoors - 1)
                if computerChoice == winningDoor:
                    stay += 1
            self.probabilityFound.append(stay / self.numberOfSimulations)

        Error = 0
        for i in range(3, DoorsLimit + 1):
            Error += ((abs(self.probabilityFound[i-3] - 1/i))*i)
        Error /= (DoorsLimit - 2)
        Error *= 100
        return str(Error) + " %"

    def graphN(self, DoorsLimit):
        expectedX = np.linspace(3, DoorsLimit, num=10 * DoorsLimit)
        expectedY = [1 / expectedX[i] for i in range(10 * DoorsLimit)]
        x = [i for i in range(3, DoorsLimit + 1)]
        plt.figure(2)
        plt.plot(x, self.probabilityFound, label="Results")
        plt.plot(expectedX, expectedY, label="Expected")
        plt.xlabel("Doors")
        plt.ylabel("Probability of success if stick")
        plt.legend()
        plt.show()


    def GameShowSim(self):
        for i in range(self.numberOfSimulations):
            position = random.randint(1, 3)
            guess = random.randint(1, 3)
            if position == guess:
                self.countOfWins += 1
            self.averagesPerIteration.append(self.countOfWins / (i + 1))
        return self.countOfWins / self.numberOfSimulations


GUI.ApplicationLoop()